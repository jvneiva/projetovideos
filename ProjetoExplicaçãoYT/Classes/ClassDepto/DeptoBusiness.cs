﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassDepto
{
    public class DeptoBusiness
    {
        public int Salvar(DeptoDTO dto)
        {
            DeptoDatabase database = new DeptoDatabase();
            return database.Salvar(dto);
        }

        public List<DeptoDTO> Listar()
        {
            DeptoDatabase database = new DeptoDatabase();
            return database.Listar();
        }

        public List<DeptoDTO> Consultar(string nome)
        {
            DeptoDatabase database = new DeptoDatabase();
            return database.Consultar(nome);
        }
    }
}
