﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassDepto
{
    public class DeptoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
