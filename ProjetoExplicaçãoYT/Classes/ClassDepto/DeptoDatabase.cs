﻿using MySql.Data.MySqlClient;
using Projeto_SIGMA.Classes.ClassesConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassDepto
{
    public class DeptoDatabase
    {
        public int Salvar(DeptoDTO dto)
        {
            string script = @"INSERT INTO tb_depto(
                            nm_depto) VALUES(
                            @nm_depto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_depto", dto.Nome));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<DeptoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_depto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DeptoDTO> lista = new List<DeptoDTO>();
            while (reader.Read())
            {
                DeptoDTO dto = new DeptoDTO();
                dto.Id = reader.GetInt32("id_depto");
                dto.Nome = reader.GetString("nm_depto");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DeptoDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_depto WHERE nm_depto LIKE @nm_depto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_depto", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DeptoDTO> lista = new List<DeptoDTO>();
            while (reader.Read())
            {
                DeptoDTO dto = new DeptoDTO();
                dto.Id = reader.GetInt32("id_depto");
                dto.Nome = reader.GetString("nm_depto");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
