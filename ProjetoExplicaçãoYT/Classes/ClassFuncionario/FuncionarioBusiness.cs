﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassFuncionario
{
    public class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            FuncionarioDatabase database = new FuncionarioDatabase();
            return database.Salvar(dto);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase database = new FuncionarioDatabase();
            database.Alterar(dto);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase database = new FuncionarioDatabase();
            return database.Listar();
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            FuncionarioDatabase database = new FuncionarioDatabase();
            return database.Consultar(nome);
        }
    }
}
