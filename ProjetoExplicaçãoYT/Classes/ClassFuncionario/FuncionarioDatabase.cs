﻿using MySql.Data.MySqlClient;
using Projeto_SIGMA.Classes.ClassesConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassFuncionario
{
    public class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO tb_funcionario(
                            nm_funcionario,
                            ds_cpf,
                            id_depto) VALUES(
                            @nm_funcionario,
                            @ds_cpf,
                            @id_depto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("id_depto", dto.DeptoId));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario SET
                            nm_funcionario = @nm_funcionario,
                            ds_cpf = @ds_cpf,
                            id_depto = @id_depto WHERE
                            id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("id_depto", dto.DeptoId));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.CPF = reader.GetString("ds_cpf");
                dto.DeptoId = reader.GetInt32("id_depto");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_funcionario LIKE @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.CPF = reader.GetString("ds_cpf");
                dto.DeptoId = reader.GetInt32("id_depto");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
