﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassLogin
{
    public class LoginBusiness
    {
        public int Salvar(LoginDTO dto)
        {
            LoginDatabase database = new LoginDatabase();
            return database.Salvar(dto);
        }

        public LoginDTO Logar(string usuario, string senha)
        {
            LoginDatabase database = new LoginDatabase();
            return database.Logar(usuario, senha);
        }
    }
}
