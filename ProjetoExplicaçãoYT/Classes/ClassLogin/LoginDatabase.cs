﻿using MySql.Data.MySqlClient;
using Projeto_SIGMA.Classes.ClassesConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassLogin
{
    public class LoginDatabase
    {
        public int Salvar(LoginDTO dto)
        {
            string script = @"INSERT INTO tb_login(
                            nm_usuario,
                            ds_senha,
                            nm_completo,
                            ds_email,
                            pr_permissaoAdm,
                            pr_permissaoCadastro,
                            pr_permissaoConsulta) VALUES(
                            @nm_usuario,
                            @ds_senha,
                            @nm_completo,
                            @ds_email,
                            @pr_permissaoAdm,
                            @pr_permissaoCadastro,
                            @pr_permissaoConsulta)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_completo", dto.Nome));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("pr_permissaoAdm", dto.PermissaoAdm));
            parms.Add(new MySqlParameter("pr_permissaoCadastro", dto.PermissaoCadastro));
            parms.Add(new MySqlParameter("pr_permissaoConsulta", dto.PermissaoConsulta));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        /*Note que a classe Alterar não retorna uma Lisa, como List<LoginDTO>, por que na verdade o que precisamos é dos
        dados de somente uma pessoa, somente um login, somente um DTO. Ao contrário do caso do Listar que retorna os dados
        de TODOS os registros na tabela, aqui precisamos de um só.*/

        public LoginDTO Logar(string usuario, string senha)
        {
            string script = @"SELECT * FROM tb_login WHERE nm_usuario = @nm_usuario AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            /*Note que ao invés de chamarmos uma Lista do DTO, chamamos a Classe LoginDTO e igualamos ela a null,
             isso por que se o Script não tiver nenhum registro que bata com a validação, Usuario = Usuário e Senha
              = Senha, o DTO instanciado será Null(vazio).*/

            LoginDTO dto = null;

            /*Note também que no lugar de um While, usamos um IF. o IF aqui tem um papel fundamental, por que ele só
             vai ser executado se a validação Usuário = Usuário e Senha = Senha for verdadeira, tornando o DTO instanciado
             com os valores dentro do IF.*/

            if (reader.Read())
            {
                dto = new LoginDTO();
                dto.Id = reader.GetInt32("id_login");
                dto.Usuario = reader.GetString("nm_usuario");
                dto.Senha = reader.GetString("ds_senha");
                dto.Nome = reader.GetString("nm_completo");
                dto.Email = reader.GetString("ds_email");
                dto.PermissaoAdm = reader.GetBoolean("pr_permissaoAdm");
                dto.PermissaoCadastro = reader.GetBoolean("pr_permissaoCadastro");
                dto.PermissaoConsulta = reader.GetBoolean("pr_permissaoConsulta");
            }

            reader.Close();
            return dto;

        }
    }
}
