﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoExplicaçãoYT.Classes.ClassLogin
{
    static class UserSession
    {
        public static LoginDTO UsuarioLogado { get; set; }
    }
}

/*O UserSession será utilizado para sabermos quem foi o Unuário logado, criando uma varíavel UsuarioLogado que 
 herdará a classe LoginDTO.*/