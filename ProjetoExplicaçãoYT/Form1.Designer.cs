﻿namespace ProjetoExplicaçãoYT
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBaixo = new System.Windows.Forms.Panel();
            this.pnlCentro = new System.Windows.Forms.Panel();
            this.btnCadastrarFuncio = new System.Windows.Forms.Button();
            this.btnConsultarFuncio = new System.Windows.Forms.Button();
            this.btnDepto = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.pnlBaixo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBaixo
            // 
            this.pnlBaixo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBaixo.Controls.Add(this.btnLogOut);
            this.pnlBaixo.Controls.Add(this.btnDepto);
            this.pnlBaixo.Controls.Add(this.btnConsultarFuncio);
            this.pnlBaixo.Controls.Add(this.btnCadastrarFuncio);
            this.pnlBaixo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBaixo.Location = new System.Drawing.Point(0, 291);
            this.pnlBaixo.Name = "pnlBaixo";
            this.pnlBaixo.Size = new System.Drawing.Size(429, 57);
            this.pnlBaixo.TabIndex = 0;
            // 
            // pnlCentro
            // 
            this.pnlCentro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCentro.Location = new System.Drawing.Point(0, 0);
            this.pnlCentro.Name = "pnlCentro";
            this.pnlCentro.Size = new System.Drawing.Size(429, 291);
            this.pnlCentro.TabIndex = 1;
            // 
            // btnCadastrarFuncio
            // 
            this.btnCadastrarFuncio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCadastrarFuncio.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrarFuncio.Location = new System.Drawing.Point(10, 3);
            this.btnCadastrarFuncio.Name = "btnCadastrarFuncio";
            this.btnCadastrarFuncio.Size = new System.Drawing.Size(144, 23);
            this.btnCadastrarFuncio.TabIndex = 0;
            this.btnCadastrarFuncio.Text = "Cadastrar Funcionário";
            this.btnCadastrarFuncio.UseVisualStyleBackColor = true;
            this.btnCadastrarFuncio.Click += new System.EventHandler(this.btnCadastrarFuncio_Click);
            // 
            // btnConsultarFuncio
            // 
            this.btnConsultarFuncio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnConsultarFuncio.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarFuncio.Location = new System.Drawing.Point(10, 30);
            this.btnConsultarFuncio.Name = "btnConsultarFuncio";
            this.btnConsultarFuncio.Size = new System.Drawing.Size(144, 23);
            this.btnConsultarFuncio.TabIndex = 1;
            this.btnConsultarFuncio.Text = "Consultar Funcionário";
            this.btnConsultarFuncio.UseVisualStyleBackColor = true;
            this.btnConsultarFuncio.Click += new System.EventHandler(this.btnConsultarFuncio_Click);
            // 
            // btnDepto
            // 
            this.btnDepto.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDepto.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepto.Location = new System.Drawing.Point(179, 10);
            this.btnDepto.Name = "btnDepto";
            this.btnDepto.Size = new System.Drawing.Size(141, 34);
            this.btnDepto.TabIndex = 2;
            this.btnDepto.Text = "Departamento";
            this.btnDepto.UseVisualStyleBackColor = true;
            this.btnDepto.Click += new System.EventHandler(this.btnDepto_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.Location = new System.Drawing.Point(353, 4);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(62, 46);
            this.btnLogOut.TabIndex = 4;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 348);
            this.Controls.Add(this.pnlCentro);
            this.Controls.Add(this.pnlBaixo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            this.pnlBaixo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBaixo;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Button btnDepto;
        private System.Windows.Forms.Button btnConsultarFuncio;
        private System.Windows.Forms.Button btnCadastrarFuncio;
        private System.Windows.Forms.Panel pnlCentro;
    }
}

