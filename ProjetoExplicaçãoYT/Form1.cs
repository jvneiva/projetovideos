﻿using ProjetoExplicaçãoYT.Classes.ClassLogin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoExplicaçãoYT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            /*O método Permissões é chamada:*/
            Permissoes();
        }

        /*O método Permissoes serve para verificar se o usuário logado possui as permissões 
         para usar om programa.*/
        void Permissoes()
        {
            /*Se o usuário logado não tiver a permissão de ADM...*/
            if (UserSession.UsuarioLogado.PermissaoAdm == false)
            {
                /*Se o usuário logado não tiver a permissão de Cadastrar...*/
                if (UserSession.UsuarioLogado.PermissaoCadastro == false)
                {
                    /*O botão cadastrar fica indisponível.*/
                    btnCadastrarFuncio.Enabled = false;
                }

                /*Se o usuário logado não tiver permissão de Consultar...*/
                if (UserSession.UsuarioLogado.PermissaoConsulta == false)
                {
                    /*O botão de consultar fica indisponível*/
                    btnConsultarFuncio.Enabled = false;
                }
            }
        }

        /*O médodo OpenScreen serve para abrir um UserControl num panel na Form principal. Ele
         recebe uma UserControl como parâmetro.*/
        void OpenScreen(UserControl control)
        {
            /*Se o panel tiver um user control aberto...*/
            if (pnlCentro.Controls.Count == 1)          
                
                /*... ele remove o UserControl ...*/
                pnlCentro.Controls.RemoveAt(0);
            /*... logo em seguida outro UserControl é adicionado.*/
          pnlCentro.Controls.Add(control);
            
        }

        private void btnCadastrarFuncio_Click(object sender, EventArgs e)
        {
            Telas.TelasRegistro.frmRegistrarFuncionario tela = new Telas.TelasRegistro.frmRegistrarFuncionario();

            /*O método OpenScreen é chamado e passa como parâmetro o UserControl instanciado.*/
            OpenScreen(tela);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Volta para a tela do Login.
            Telas.TelasDeLogin.frmLogin tela = new Telas.TelasDeLogin.frmLogin();
            tela.Show();
            this.Close();
        }

        private void btnConsultarFuncio_Click(object sender, EventArgs e)
        {
            Telas.TelasConsulta.frmConsultarFuncionario tela = new Telas.TelasConsulta.frmConsultarFuncionario();
            /*O método OpenScreen é chamado e passa como parâmetro o UserControl instanciado.*/
            OpenScreen(tela);
        }

        private void btnDepto_Click(object sender, EventArgs e)
        {
            Telas.TelasRegistro.frmRegistrarDepto depto = new Telas.TelasRegistro.frmRegistrarDepto();
            /*O método OpenScreen é chamado e passa como parâmetro o UserControl instanciado.*/
            OpenScreen(depto);
        }
    }
}
