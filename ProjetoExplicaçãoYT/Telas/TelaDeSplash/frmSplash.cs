﻿using ProjetoExplicaçãoYT.Telas.TelasDeLogin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoExplicaçãoYT.Telas.TelaDeSplash
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();

            /*Iniciar a contagem do Timer assim que a tela iniciar*/

            TimerProgress.Start();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {

        }

        private void TimerProgress_Tick(object sender, EventArgs e)
        {
            /*O progressBar vai almentando 1 por 1 conforme o Timer for andando.*/

            ProgressCarregar.Increment(1);

            /*Se o ProgressBar atingir 100% do seu valor...*/
            if (ProgressCarregar.Value == 100)
            {

                /*... ele para o Timer e abre a tela de Login.
                 Note também que ele usua o HIDE, ou seja, a tela não será fechada.*/
                TimerProgress.Stop();
                frmLogin tela = new frmLogin();
                tela.Show();
                this.Hide();
            }
        }

        private void ProgressCarregar_Click(object sender, EventArgs e)
        {

        }
    }
}
