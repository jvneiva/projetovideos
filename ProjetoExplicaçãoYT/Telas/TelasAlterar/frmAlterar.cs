﻿using ProjetoExplicaçãoYT.Classes.ClassDepto;
using ProjetoExplicaçãoYT.Classes.ClassFuncionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoExplicaçãoYT.Telas.TelasAlterar
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            DeptoDTO depto = new DeptoDTO();
            DeptoBusiness business = new DeptoBusiness();

            List<DeptoDTO> lista = business.Listar();

            cboDepto.ValueMember = nameof(depto.Id);
            cboDepto.DisplayMember = nameof(depto.Nome);

            cboDepto.DataSource = lista;
        }

        /*O DTO do funcioário é instanciado como escopo para ter os dados salvos já, recebendo depois
         somente os dados alterados.*/

        FuncionarioDTO dto;

        /*Este método serve para carregar todos os campos da tela com o DTO que será editado,
         ou seja, os controles da tela já veem preenchidos, basta somente alterar aquilo que precisa.*/

        public void LoadScreen(FuncionarioDTO dto)
        {
            this.dto = dto;
            txtNome.Text = dto.Nome;
            mkbCPF.Text = dto.CPF;
            cboDepto.SelectedItem = dto.DeptoId;
        }


        private void btnAlterar_Click(object sender, EventArgs e)
        {
            DeptoDTO Depto = cboDepto.SelectedItem as DeptoDTO;

            /*A palavra This é usada para acessar o DTO escopo de classe acima.*/
            this.dto.Nome = txtNome.Text;
            this.dto.CPF = mkbCPF.Text;
            this.dto.DeptoId = Depto.Id;

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Alterar(dto);

            MessageBox.Show("Funcionario Alterado.");
        }
    }
}
