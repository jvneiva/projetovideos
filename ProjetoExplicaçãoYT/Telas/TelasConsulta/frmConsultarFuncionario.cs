﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoExplicaçãoYT.Classes.ClassFuncionario;

namespace ProjetoExplicaçãoYT.Telas.TelasConsulta
{
    public partial class frmConsultarFuncionario : UserControl
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
            AutoCarregar();
        }

        void AutoCarregar()
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            FuncionarioBusiness business = new FuncionarioBusiness();

            List<FuncionarioDTO> lista = business.Listar();

            dgvConsultarFuncio.AutoGenerateColumns = false;
            dgvConsultarFuncio.DataSource = lista;
        }

        void CarregarGrid()
        {
            string buscar = txtNome.Text;

            FuncionarioDTO dto = new FuncionarioDTO();
            FuncionarioBusiness business = new FuncionarioBusiness();

            List<FuncionarioDTO> lista = business.Consultar(buscar);

            dgvConsultarFuncio.AutoGenerateColumns = false;
            dgvConsultarFuncio.DataSource = lista;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvConsultarFuncio_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*Se a coluna número 4 for clicada...*/

            if (e.ColumnIndex == 4)
            {
                /*... o DTO do funcionário é instanciado e igualado aos registros selecionados na linha clicada.*/

                FuncionarioDTO dto = dgvConsultarFuncio.Rows[e.RowIndex].DataBoundItem as FuncionarioDTO;

                Telas.TelasAlterar.frmAlterar tela = new TelasAlterar.frmAlterar();

                /*O método LoadScreen da tela de alterar é chamado para enviar os dados da linha dentro da variável
                 dto do FuncionarioDTO para a tela.*/
                tela.LoadScreen(dto);
                tela.Show();
            }
        }
    }
}
