﻿using ProjetoExplicaçãoYT.Classes.ClassLogin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoExplicaçãoYT.Telas.TelasDeLogin
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginDTO dto = new LoginDTO();
            dto.Nome = txtNome.Text;
            dto.Email = txtEmail.Text;
            dto.Usuario = txtUsuario.Text;
            dto.Senha = txtSenha.Text;
            dto.PermissaoAdm = ckbAdm.Checked;
            dto.PermissaoCadastro = ckbCadastar.Checked;
            dto.PermissaoConsulta = ckbConsultar.Checked;

            /*As permissões são salvar no banco como Boolean.*/

            LoginBusiness business = new LoginBusiness();
            business.Salvar(dto);

            frmLogin tela = new frmLogin();
            tela.Show();
            this.Close();
        }
    }
}
