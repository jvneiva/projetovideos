﻿using ProjetoExplicaçãoYT.Classes.ClassLogin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoExplicaçãoYT.Telas.TelasDeLogin
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            LoginBusiness business = new LoginBusiness();
            string user = txtUser.Text;
            string pass = txtPass.Text;

            /*o DTO é igualado ao método Logar da Database, passando somente os dados
             validos para a variável usuario.*/

            LoginDTO usuario = business.Logar(user, pass);

            /*Se usuario for diferente de vazio...*/
            if (usuario != null)
            {
                /*... Os dados contidos na variável usuario são transferidas para a Classe UserSession.*/
                UserSession.UsuarioLogado = usuario;

                Form1 tela = new Form1();
                tela.Show();
                this.Close();
            }
            else
            {
                /*Caso o usuário esteja nulo, cairá aqui dentro do else.*/

                MessageBox.Show("Nome de usuário ou senha incorretos.");
            }

          
        }

        private void lblRegistrar_Click(object sender, EventArgs e)
        {
            frmCadastrar tela = new frmCadastrar();
            tela.Show();
            this.Close();
        }
    }
}
