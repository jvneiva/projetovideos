﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoExplicaçãoYT.Classes.ClassDepto;

namespace ProjetoExplicaçãoYT.Telas.TelasRegistro
{
    public partial class frmRegistrarDepto : UserControl
    {
        public frmRegistrarDepto()
        {
            InitializeComponent();

            /*Ao abrir a tela, o método abaixo é chamada.*/
            AutoCarregar();
        }

        /*O método AutoCarregar serve para que a Grid já venha carregada ao abrir a tela.*/
        void AutoCarregar()
        {
            DeptoDTO dto = new DeptoDTO();
            DeptoBusiness business = new DeptoBusiness();

            /*A lista de DeptoDTO é preenchida com todos os dados da tabela de departamento.*/
            List<DeptoDTO> lista = business.Listar();

            dgvConsultarDepto.AutoGenerateColumns = false;

            /*A Grid é carregada com os dados da lista.*/
            dgvConsultarDepto.DataSource = lista;
        }


        /*O método CarregarGrid serve para as consultas que o usuário fizer ao clicar no botão de consultar*/
        void CarregarGrid()
        {
            /*uma variável guarda a informação no textbox*/
            string buscar = txtConsultar.Text;

            DeptoDTO dto = new DeptoDTO();
            DeptoBusiness business = new DeptoBusiness();

            /*A lista de DeptoDTO é preenchida com todos os dados da tabela de departamento que foram consultada a partir
            da variável.*/
            List<DeptoDTO> lista = business.Consultar(buscar);

            dgvConsultarDepto.AutoGenerateColumns = false;

            /*A Grid é carregada com os dados da lista.*/
            dgvConsultarDepto.DataSource = lista;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            DeptoDTO dto = new DeptoDTO();
            dto.Nome = txtCadastrar.Text;

            DeptoBusiness business = new DeptoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Departamento salvo.");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
