﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoExplicaçãoYT.Classes.ClassDepto;
using ProjetoExplicaçãoYT.Classes.ClassFuncionario;

namespace ProjetoExplicaçãoYT.Telas.TelasRegistro
{
    public partial class frmRegistrarFuncionario : UserControl
    {
        public frmRegistrarFuncionario()
        {
            InitializeComponent();

            /*As combos são carregadas assim que a tela é aberta a partir do método abaixo:*/
            CarregarCombos();
        }

        /*O método CarregarCombos serve para que as combos sejam carregadas corretamente a partir da 
         DTO de outra tabela, ou da mesma tabela.*/
        void CarregarCombos()
        {
            DeptoDTO depto = new DeptoDTO();
            DeptoBusiness business = new DeptoBusiness();

            /*A lista é preenchida com os dados listados da Business.*/
            List<DeptoDTO> lista = business.Listar();

            /*Aqui é determinado o valor que o combo representará, no caso, o Id do departamento*/
            cboDepto.ValueMember = nameof(depto.Id);

            /*Aqui é oque que vai aparecer ao clicar na combo, no caso, serão listados todos os nomes
             de departamento.*/
            cboDepto.DisplayMember = nameof(depto.Nome);

            /*Aqui é oque o combo vai listar, no caso é toda a lista da tabela do departamento, porém, os
             dois últimos comandos servem como um filtro do que a combo deve listar.*/
            cboDepto.DataSource = lista;
        }

        /*CHAVE ESTRANGEIRA*/
        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            /*DeptoDTO depto é igual ao item selecionado do ComboBox convertido numa DeptoDTO.
             
             o DTO do deparamento terá as informações contida no item do ComboBox selecionado.*/
            DeptoDTO depto = cboDepto.SelectedItem as DeptoDTO;

            FuncionarioDTO dto = new FuncionarioDTO();
            dto.Nome = txtNome.Text;
            dto.CPF = mkbCPF.Text;

            /*A coluna DeptoId da variável dto (FuncionariDTO) instanciado acima será igualada a
             variável depto(DeptoDTO) .Id, ou seja, a coluna DeptoID é igual ao Id do departamento que foi
             instanciado lá em cima.*/
            dto.DeptoId = depto.Id;

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Salvar(dto);

            MessageBox.Show("Funcionario cadastrado.");
        }

        private void cboDepto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
